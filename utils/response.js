var defaultMessage = 'Unexpected Error Occurred';
var statusOk = 'OK';
var statusError = 'ERROR';

// If success
var ok = function(body, response) {
    return {
        Status: statusOk,
        StatusCode: response && response.statusCode,
        Data: body,
    };
};

// If error
var error = function(error, response, msg = defaultMessage) {
    return {
        Status: statusError,
        StatusCode: response && response.statusCode,
        Message: msg,
        Data: error,
        Code: error && error.code,
    };
};

// If server error
var serverError = function(body, response) {
    return {
        Status: statusError,
        StatusCode: response && response.statusCode,
        Message: defaultMessage,
        Data: body,
    };
};

var getErrorSFDC = function(body) {
    var error = null;

    try {
        var _body = body && typeof body === 'string' && JSON.parse(body);
        // old
        if(_body && _body.error) {
            error = { value: _body.stackTrace, msg: _body.error }
        }

        // in case sfdc side return Response type
        if(_body && _body.status === 'error') {
            error = { value: _body.stackTrace, msg: _body.messages[0].message }
        }
    } catch (e) {
        console.log(e);
    }

    return error;
};

module.exports = function(err, response, body) {
    var result;

    if(err) result = error(err, response);
    else if(response && response.statusCode >= 500) result = serverError(body, response);
    else if(response && response.statusCode >= 400) result = error(body, response);
    else result = ok(body, response);

    // In case sfdc error
    var err = getErrorSFDC(body);
    if(err) {
        result = error(err.value, response, err.msg);
    }

    return result;
}
