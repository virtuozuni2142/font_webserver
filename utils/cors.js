const cors = require('cors');

var allowedRequesters = ['.visual.force.com', '.visualforce.com', '.force.com'];

module.exports = cors((req, callback) => {
    var corsOptions;
    const origin = req.header('Origin');
    const isAllowed = allowedRequesters.some(ar => origin && origin.endsWith(ar));
    if (isAllowed) {
      corsOptions = { origin: true, credentials: true }
    } else {
      corsOptions = { origin: false }
    }
    callback(null, corsOptions)
});
